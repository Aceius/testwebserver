# TestWebServer
Little simple webserver I made in an hour or so using C# because I got bored.

## Usage
1. Clone the repo
2. Change `Constants.cs`
3. Run `dotnet publish`
4. Copy the `TestWebServer.dll` to some directory and `dotnet run TestWebServer.dll`
5. Add your website to `www/` (or whatever you set WEBROOT to). 

Note that this webserver is very simple, certainly should not be used in
production, and only supports text files.

## Licence
Licenced under the Opinionated Queer License, see `Licence.md`.

I was going to dedicate this software to the public domain but then I decided
that I wanted to use some random obscure licence instead. You should not be even
using this software anyway.