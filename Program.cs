﻿using System.Net;
using System.Text;
using static Constants;

string getFilepath(HttpListenerRequest request)
{
    if (request.RawUrl is not null)
    {
        string filepath = request.RawUrl;

        // Remove query parameters (?foo=bar) 
        int queryStringIndex = filepath.IndexOf('?');

        bool urlContainsQueryString = queryStringIndex != -1;
        if (urlContainsQueryString) filepath = filepath.Substring(0, queryStringIndex);

        filepath = Path.Join(WEBROOT, filepath);

        return Path.GetFullPath(filepath);
    }
    else
    {
        return "/";
    }
}

string getMimetype(string filepath)
{
    string extension = filepath.Split('.').Last();
    string mimetype = extension switch
    {
        "html" => "text/html",
        "json" => "application/json",
        _ => "text/plain",
    }; // TODO: Add more mimetypes...

    return mimetype;
}

string doError(HttpListenerResponse response, int errorCode)
{
    response.AddHeader("Content-Type", "text/html");
    response.StatusCode = errorCode;

    return $"<!doctype HTML><html><head><title>Error {errorCode}</title></head><body style=\"{BODY_STYLES}\"><h1 style=\"{H1_STYLES}\">Error {errorCode}</h1><p>Contact the webmaster if you beleive this to be in error. <a href=\"/\" style=\"{LINK_STYLES}\">Back to home</a>.</p></body></html>";
}

Console.WriteLine("Starting app.");

if (!Directory.Exists(WEBROOT)) Directory.CreateDirectory(WEBROOT);

if (!HttpListener.IsSupported)
{
    Console.WriteLine("HttpListener is not supported on this platform.");
    return;
}

var listener = new HttpListener();
listener.Prefixes.Add(LISTEN_URL);

Console.WriteLine($"Listening on port {LISTEN_PORT}");
listener.Start();

bool shutdownNotScheduled = true;
while (shutdownNotScheduled)
{
    var context = listener.GetContext();
    var request = context.Request;
    var response = context.Response;
    string filepath = getFilepath(request);

    // request.QueryString = $_GET

    bool directoryRequested = filepath.Last() == '/' || filepath.Last() == '\\';
    if (directoryRequested) filepath = Path.Join(filepath, "index.html");

    string plaintextResponse;

    if (File.Exists(filepath))
    {
        response.AddHeader("Content-Type", getMimetype(filepath));
        plaintextResponse = File.ReadAllText(filepath);
    }
    else if (filepath == Path.GetFullPath(Path.Join(WEBROOT, "index.html")))
    {
        response.AddHeader("Content-Type", "text/html");
        var stringBuilder = new StringBuilder();

        stringBuilder.AppendLine($"<!doctype HTML><html><head><title>TestWebServer is functional</title></head><body style=\"{BODY_STYLES}\">");
        stringBuilder.AppendLine($"<h1 style=\"{H1_STYLES}\">TestWebServer is now running on port {LISTEN_PORT}</h1>");
        stringBuilder.AppendLine($"<p>Your webroot is: <code>{Path.GetFullPath(WEBROOT)}</code></p>");
        stringBuilder.AppendLine($"<p>Create an index.html file in that folder to replace this message.</p>");
        stringBuilder.AppendLine("</body></html>");

        plaintextResponse = stringBuilder.ToString();
    }
    else if (directoryRequested)
    {
        plaintextResponse = "directory listing";
    }
    else
    {
        plaintextResponse = doError(response, 404);
    }

    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(plaintextResponse);
    response.ContentLength64 = buffer.Length;
    response.OutputStream.Write(buffer, 0, buffer.Length);
    response.OutputStream.Close();

    Console.WriteLine($"[{DateTime.Now}] {request.HttpMethod} {request.Url} -> {filepath} ... {response.StatusCode}");
}

Console.WriteLine("Stopping program.");
listener.Stop();