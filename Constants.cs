static class Constants
{
    public const int LISTEN_PORT = 4568;
    public static readonly string LISTEN_URL = $"http://localhost:{LISTEN_PORT}/";
    public const string WEBROOT = "www/";
    public const string BODY_STYLES = "background-color:#111;color:#eee;font-family:system-ui,sans-serif;max-width:800px;margin:auto;";
    public const string H1_STYLES = "background-color:#17a;text-align:center;";
    public const string LINK_STYLES = "color:#17a !important; ";
}